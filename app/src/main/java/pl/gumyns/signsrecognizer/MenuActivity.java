package pl.gumyns.signsrecognizer;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemLongClick;
import pl.gumyns.signsrecognizer.neural.EncogNetwork;
import pl.gumyns.signsrecognizer.opencv.ImageManipulation;
import rx.Subscriber;
import rx.Subscription;


public class MenuActivity extends Activity implements SurfaceHolder.Callback {

    @InjectView(R.id.imageView2)
    ImageView upper;

    @InjectView(R.id.textView)
    TextView textView;

    @InjectView(R.id.listView)
    ListView listView;

    @InjectView(R.id.surfaceView)
    SurfaceView surfaceView;

    @InjectView(R.id.speedLayout)
    FrameLayout speedLayout;
    @InjectView(R.id.speedTextView)
    TextView speedTextView;

    EncogNetwork encogNetwork;
    List<Bitmap> bitmapList;
    ImageListAdapter adapter;
    Subscription subscription = null;
    private Bitmap bitmap = null;
    private SurfaceHolder sHolder;
    //a variable to control the camera
    private Camera camera;
    //the camera parameters
    private Camera.Parameters parameters;
    //open cv utils
    private ImageManipulation imageManipulation;
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i("OpenCV", "OpenCV loaded successfully");
                    imageManipulation = new ImageManipulation();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.inject(this);
        speedLayout.setVisibility(View.GONE);
        sHolder = surfaceView.getHolder();
        sHolder.addCallback(this);
        sHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        bitmapList = new ArrayList<Bitmap>();
        adapter = new ImageListAdapter(this, bitmapList);
        listView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, mLoaderCallback);
        encogNetwork = new EncogNetwork(this);

    }

    @OnClick(R.id.button)
    void getPhoto(View v) {
        camera.takePicture(null, null, new Camera.PictureCallback()
        {
            @Override
            public void onPictureTaken(byte[] data, Camera camera)
            {

                textView.setText("");
                //decode the data obtained by the camera into a Bitmap
                bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                imageManipulation.setImage(bitmap);
                try {
                    upper.setImageBitmap(imageManipulation.getImage());
                } catch (Exception e) {}
                if(subscription != null && !subscription.isUnsubscribed())
                    subscription.unsubscribe();
                subscription = imageManipulation.processCircles().subscribe(new Subscriber<List<Bitmap>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Bitmap> bitmaps) {
                        bitmapList = bitmaps;
                        String speed = encogNetwork.run(bitmapList);
                        textView.setText("Odczytana predkosc: " + speed);
                        adapter = new ImageListAdapter(MenuActivity.this, bitmapList);
                        listView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        if(speed.endsWith("0")) {
                            speedTextView.setText(encogNetwork.run(bitmapList));
                            speedLayout.setVisibility(View.VISIBLE);
                        }
                    }
                });
                camera.startPreview();
                //getPhoto(null);
            }
        });
    }

    @OnClick(R.id.speedLayout)
    void resetClick(View v){
        speedLayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.button3)
    void testOpenCv(View v) {
        imageManipulation.setImage(BitmapFactory.decodeResource(getResources(), R.drawable.znaktestowy));
        if(subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();
        imageManipulation.processCircles().subscribe(new Subscriber<List<Bitmap>>() {
            @Override
            public void onCompleted() {
                upper.setImageBitmap(imageManipulation.getImage());
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<Bitmap> bitmaps) {
                bitmapList = bitmaps;
                textView.setText("Odczytana predkosc: " + encogNetwork.run(bitmapList));
                adapter = new ImageListAdapter(MenuActivity.this, bitmapList);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                speedTextView.setText(encogNetwork.run(bitmapList));
                speedLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    @OnItemLongClick(R.id.listView)
    boolean saveImage(AdapterView<?> adapterView, View view, int pos, long x) {
        Bitmap finalBitmap = bitmapList.get(pos);
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/sings");
        myDir.mkdirs();
        long n = System.currentTimeMillis();
        String fname = "Image-"+ n +".jpg";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        Toast.makeText(this, "Image saved", Toast.LENGTH_SHORT).show();
        return true;
    }



    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        camera = Camera.open();
        try {
            camera.setPreviewDisplay(sHolder);
        } catch (IOException exception) {
            camera.release();
            camera = null;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        parameters = camera.getParameters();
        parameters.setRotation(90);

        List<Camera.Size> sizes = parameters.getSupportedPictureSizes();
        Camera.Size size = sizes.get(0);
        for (int x = 0; x < sizes.size(); x++) {
            Log.d("Sizes", sizes.get(x).width +", " +sizes.get(x).height);
        }

        parameters.setPictureSize(1280, 960);
        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        camera.setParameters(parameters);
        camera.startPreview();
//        getPhoto(null); // start photo capturing
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        camera.stopPreview();
        camera.release();
        camera = null;
    }

    @Override
    protected void onDestroy() {
        imageManipulation.finalize();
        encogNetwork.finalize();
        super.onDestroy();
    }
}
