package pl.gumyns.signsrecognizer.neural;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLData;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.List;

import pl.gumyns.neural.teacher.model.NetworkData;
import pl.gumyns.signsrecognizer.imagetools.BitmapToArrayConverter;

/**
 * Created by gumyns on 2014-11-08.
 */
public class EncogNetwork {
    BasicNetwork network;
    NetworkData networkData = null;

    public EncogNetwork(Context context) {
        try {
            InputStream is = context.getAssets().open("neural.ser");
            ObjectInputStream ois = new ObjectInputStream(is);
            networkData = (NetworkData) ois.readObject();
            ois.close();
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        network = new BasicNetwork();
        network.addLayer(new BasicLayer(null,true,32*32));
        network.addLayer(new BasicLayer(new ActivationSigmoid(),true, 128));
        network.addLayer(new BasicLayer(new ActivationSigmoid(),false, networkData.getOutput().length));
        network.getStructure().finalizeStructure();
        network.reset();
        network.decodeFromArray(networkData.getNetwork());
    }

    public String run(List<Bitmap> bitmapList) {
        StringBuilder out = new StringBuilder();
        char data;
        MLData output;
        for(Bitmap bitmap: bitmapList) {
            output = network.compute(new BasicMLData(BitmapToArrayConverter.toArray(bitmap)));
            data = encodeData(output.getData());
            if(data != 0)
                out.append(data);
        }
        return out.toString();
    }

    private char encodeData(double [] data) {
        Log.d("wynik", Arrays.toString(data));
        double maxValue = 0;
        int max = 0;
        for (int i = 0; i < data.length; i++)
            if (data[i] > maxValue) {
                maxValue = data[i];
                max = i;
            }
        return networkData.getOutput()[max];
    }

    public void finalize() {
        Encog.getInstance().shutdown();
    }
}