package pl.gumyns.signsrecognizer;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by gumyns on 2014-12-03.
 */
public class ImageListAdapter extends ArrayAdapter<Bitmap> {
    Context context;

    public ImageListAdapter(Context context, List<Bitmap> objects) {
        super(context, R.layout.list_item, objects);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ImageView image = (ImageView) inflater.inflate(R.layout.list_item, parent, false);
        image.setImageBitmap(getItem(position));

        return image;
    }
}
