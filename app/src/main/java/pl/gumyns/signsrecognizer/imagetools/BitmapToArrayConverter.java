package pl.gumyns.signsrecognizer.imagetools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;

/**
 * Created by gumyns on 2014-11-08.
 */
public class BitmapToArrayConverter {

    public static double [] toArray(final Context context, final int resId) {
        return toArray(BitmapFactory.decodeResource(context.getResources(), resId));
    }

    public static double [] toArray(final Bitmap bitmap) {
        double [] array = new double[bitmap.getHeight() * bitmap.getWidth()];
        double pixel;
        int px;
        for(int i = 0; i < bitmap.getWidth(); i++) {
            for(int j = 0; j < bitmap.getHeight(); j++) {
                px = bitmap.getPixel(j,i);
                pixel = ((Color.red(px) + Color.green(px) + Color.blue(px)) / 3) / 256.0;
//                Log.d("Pixel", "(" + i + "," + j +") = " + (pixel > 0.5 ? 1.0: 0.0));
                array[i*bitmap.getWidth() + j] = pixel > 0.5 ? 1.0: 0.0;
            }
        }
        return array;
    }
}
