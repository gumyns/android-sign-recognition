package pl.gumyns.neural.teacher.neural;

/**
 * Created by gumyns on 2014-11-08.
 */
public class NeuralHelper {
    public static double [] getIdeal(int size, int index) {
        double [] table = new double[size];
        for (int i = 0; i < table.length; i++)
            table[i] = i == index ? 1.0 : 0.0;
        return table;
    }
}
